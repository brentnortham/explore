/* eslint vars-on-top: "off" */
process.env.NODE_ENV = 'test';

var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../../server/app');
var models = require('../../server/models/index');
var should = chai.should();

var PEOPLE_PATH_BASE = '/api/v1/people';
var USERS_PATH_BASE = '/api/v1/users';
var token = '';

chai.use(chaiHttp);

describe('people', function () {

  before(function(done) {
    this.timeout(10000);
    models.Person.destroy(
      {
        where: {}
      }
    ).then(function() {
      models.User.destroy(
        {
          where: {}
        }
      ).then(function(err, res) {
        chai.request(server)
          .post(USERS_PATH_BASE)
          .send({'username': 'testuser@testdomain.com', 'password': 'Test'})
          .end(function(err, res) {
            chai.request(server)
              .post(USERS_PATH_BASE + '/authenticate')
              .send({'username': 'testuser@testdomain.com', 'password': 'Test'})
              .end(function(err, res) {
                token = res.body.token;
                done();
              });
          });
      });
    });
  });

  beforeEach(function(done) {
    models.Person.create({
      firstName: 'Test',
      lastName: 'Person'
    }).then(function() {
      done();
    });
  });

  after(function(done) {
    models.User.destroy(
      {
        where: {}
      }
    ).then(function() {
      done();
    });
  });

  afterEach(function(done) {
    models.Person.destroy({
      where: {}
    }).then(function () {
      done();
    });
  });

  it('should list ALL people on ' + PEOPLE_PATH_BASE + ' GET', function(done) {
    chai.request(server)
      .get(PEOPLE_PATH_BASE)
      .set('Authorization', token)
      .end(function(err, res) {
        res.should.have.status(200);
        res.should.be.json;
        res.body.should.be.a('array');
        res.body[0].should.be.a('object');
        res.body[0].should.have.property('id');
        res.body[0].should.have.property('firstName');
        res.body[0].should.have.property('lastName');
        res.body[0].should.have.property('createdAt');
        res.body[0].should.have.property('updatedAt');
        res.body[0].firstName.should.equal('Test');
        res.body[0].lastName.should.equal('Person');
        done();
      });
  });

  it('should not list ALL people if not authorized on ' + PEOPLE_PATH_BASE + ' GET', function(done) {
    chai.request(server)
      .get(PEOPLE_PATH_BASE)
      .end(function(err, res) {
        res.should.have.status(401);
        done();
      });
  });

  it('should not list ALL people if bad token on ' + PEOPLE_PATH_BASE + ' GET', function(done) {
    chai.request(server)
      .get(PEOPLE_PATH_BASE)
      .set('Authorization', 'abcdefghijklmnopqrstuvwxyz')
      .end(function(err, res) {
        res.should.have.status(401);
        done();
      });
  });

  it('should list a SINGLE person on ' + PEOPLE_PATH_BASE + '/:id GET', function(done) {
    models.Person.create({
      firstName: 'TestCreate',
      lastName: 'Person'
    }).then(function(person) {
      chai.request(server)
        .get(PEOPLE_PATH_BASE + '/' + person.id)
        .set('Authorization', token)
        .end(function(err, res) {
          res.should.have.status(200);
          res.should.be.json;
          res.body.should.be.a('object');
          res.body.should.have.property('id');
          res.body.should.have.property('firstName');
          res.body.should.have.property('lastName');
          res.body.should.have.property('createdAt');
          res.body.should.have.property('updatedAt');
          res.body.firstName.should.equal('TestCreate');
          res.body.lastName.should.equal('Person');
          res.body.id.should.equal(person.id);
          done();
        });
    });
  });

  it('should not find a non-existent SINGLE person on ' + PEOPLE_PATH_BASE + '/:id GET', function(done) {
    models.Person.create({
      firstName: 'TestCreate',
      lastName: 'Person'
    }).then(function(person) {
      chai.request(server)
        .get(PEOPLE_PATH_BASE + '/' + person.id + 1)
        .set('Authorization', token)
        .end(function(err, res) {
          res.should.have.status(404);
          done();
        });
    });
  });

  it('should add a SINGLE person on ' + PEOPLE_PATH_BASE + ' POST', function(done) {
    chai.request(server)
      .post(PEOPLE_PATH_BASE)
      .set('Authorization', token)
      .send({'firstName': 'Brent', 'lastName': 'Test'})
      .end(function(err, res) {
        res.should.have.status(201);
        res.should.have.header('location', '/api/v1/people/' + res.body.id);
        res.body.should.be.a('object');
        res.body.should.have.property('id');
        res.body.should.have.property('firstName');
        res.body.should.have.property('lastName');
        res.body.should.have.property('createdAt');
        res.body.should.have.property('updatedAt');
        res.body.firstName.should.equal('Brent');
        res.body.lastName.should.equal('Test');
        done();
      });
  });

  it('should not add a SINGLE person that already exists on ' + PEOPLE_PATH_BASE + ' POST', function(done) {
    chai.request(server)
      .post(PEOPLE_PATH_BASE)
      .set('Authorization', token)
      .send({'firstName': 'Brent', 'lastName': 'Test'})
      .end(function(err, res) {
        chai.request(server)
          .post(PEOPLE_PATH_BASE)
          .set('Authorization', token)
          .send({
            'id': res.body.id,
            'firstName': 'AnotherBrent',
            'lastName': 'AnotherTest'
          })
          .end(function(error, response) {
            response.should.have.status(409);
            done();
          });
      });
  });

  it('should not add a SINGLE invalid person on ' + PEOPLE_PATH_BASE + ' POST', function(done) {
    chai.request(server)
      .post(PEOPLE_PATH_BASE)
      .set('Authorization', token)
      .send({'firstName': 'Brent'})
      .end(function(err, res) {
        res.should.have.status(400);
        done();
      });
  });

  it('should replace a SINGLE person on ' + PEOPLE_PATH_BASE + '/:id PUT', function(done) {
    chai.request(server)
      .get(PEOPLE_PATH_BASE)
      .set('Authorization', token)
      .end(function(err, res) {
        chai.request(server)
          .put(PEOPLE_PATH_BASE + '/' + res.body[0].id)
          .set('Authorization', token)
          .send({
            'firstName': 'Spider',
            'lastName': 'Monkey'
          })
          .end(function(error, response) {
            response.should.have.status(200);
            response.should.be.json;
            response.body.should.be.a('object');
            response.body.should.have.property('id');
            response.body.should.have.property('firstName');
            response.body.should.have.property('lastName');
            response.body.should.have.property('createdAt');
            response.body.should.have.property('updatedAt');
            response.body.firstName.should.equal('Spider');
            response.body.lastName.should.equal('Monkey');
            done();
          });
      });
  });

  it('should not replace a non-existent SINGLE person on ' + PEOPLE_PATH_BASE + '/:id PUT', function(done) {
    chai.request(server)
      .get(PEOPLE_PATH_BASE)
      .set('Authorization', token)
      .end(function(err, res) {
        chai.request(server)
          .put(PEOPLE_PATH_BASE + '/' + res.body[0].id + 1)
          .set('Authorization', token)
          .send({
            'firstName': 'Spider',
            'lastName': 'Monkey'
          })
          .end(function(error, response) {
            response.should.have.status(404);
            done();
          });
      });
  });

  it('should update individual values for a SINGLE person on ' + PEOPLE_PATH_BASE + '/:id PATCH', function(done) {
    chai.request(server)
      .get(PEOPLE_PATH_BASE)
      .set('Authorization', token)
      .end(function(err, res) {
        chai.request(server)
          .patch(PEOPLE_PATH_BASE + '/' + res.body[0].id)
          .set('Authorization', token)
          .send({
            'firstName': 'Spider'
          })
          .end(function(error, response) {
            response.should.have.status(200);
            response.should.be.json;
            response.body.should.be.a('object');
            response.body.should.have.property('id');
            response.body.should.have.property('firstName');
            response.body.should.have.property('lastName');
            response.body.should.have.property('createdAt');
            response.body.should.have.property('updatedAt');
            response.body.firstName.should.equal('Spider');
            done();
          });
      });
  });

  it('should not update non-provided values for a SINGLE person on ' + PEOPLE_PATH_BASE + '/:id PATCH', function(done) {
    chai.request(server)
      .get(PEOPLE_PATH_BASE)
      .set('Authorization', token)
      .end(function(err, res) {
        chai.request(server)
          .patch(PEOPLE_PATH_BASE + '/' + res.body[0].id)
          .set('Authorization', token)
          .send({
            'firstName': 'Spider'
          })
          .end(function(error, response) {
            response.should.have.status(200);
            response.should.be.json;
            response.body.should.be.a('object');
            response.body.should.have.property('id');
            response.body.should.have.property('firstName');
            response.body.should.have.property('lastName');
            response.body.should.have.property('createdAt');
            response.body.should.have.property('updatedAt');
            response.body.firstName.should.equal('Spider');
            response.body.lastName.should.equal(res.body[0].lastName);
            done();
          });
      });
  });

  it('should not update individual values for a non-existent SINGLE person on ' + PEOPLE_PATH_BASE + '/:id PATCH', function(done) {
    chai.request(server)
      .get(PEOPLE_PATH_BASE)
      .set('Authorization', token)
      .end(function(err, res) {
        chai.request(server)
          .patch(PEOPLE_PATH_BASE + '/' + res.body[0].id + 1)
          .set('Authorization', token)
          .send({
            'firstName': 'Spider'
          })
          .end(function(error, response) {
            response.should.have.status(404);
            done();
          });
      });
  });

  it('should delete a SINGLE person on ' + PEOPLE_PATH_BASE + '/:id DELETE', function(done) {
    chai.request(server)
      .get(PEOPLE_PATH_BASE)
      .set('Authorization', token)
      .end(function(err, res) {
        chai.request(server)
          .delete(PEOPLE_PATH_BASE + '/' + res.body[0].id)
          .set('Authorization', token)
          .end(function(error, response) {
            response.should.have.status(200);
            response.should.be.json;
            response.body.should.equal(1);
            done();
          });
      });
  });

  it('should not delete a non-existent SINGLE person on ' + PEOPLE_PATH_BASE + '/:id DELETE', function(done) {
    chai.request(server)
      .get(PEOPLE_PATH_BASE)
      .set('Authorization', token)
      .end(function(err, res) {
        chai.request(server)
          .delete(PEOPLE_PATH_BASE + '/' + res.body[0].id + 1)
          .set('Authorization', token)
          .end(function(error, response) {
            response.should.have.status(404);
            done();
          });
      });
  });

  it('should not allow other http methods', function(done) {
    chai.request(server)
      .options(PEOPLE_PATH_BASE)
      .end(function(err, res) {
        res.should.have.status(405);
        res.should.have.header('allow', 'GET, POST, PATCH, DELETE');
      });
    chai.request(server)
      .trace(PEOPLE_PATH_BASE)
      .end(function(err, res) {
        res.should.have.status(405);
        res.should.have.header('allow', 'GET, POST, PATCH, DELETE');
      });
    done();
  });
});
