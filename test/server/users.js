/* eslint vars-on-top: "off" */
process.env.NODE_ENV = 'test';

var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../../server/app');
var models = require('../../server/models/index');
var should = chai.should();

var USERS_PATH_BASE = '/api/v1/users';
var token = '';

chai.use(chaiHttp);

describe('users', function() {

  before(function(done) {
    models.User.destroy({
      where: {}
    }).then(function() {
      done();
    });
  });

  afterEach(function(done) {
    models.User.destroy({
      where: {}
    }).then(function () {
      done();
    });
  });

  it('should hash the password of a new user model', function(done) {
    models.User.create({
      username: 'testuser@testdomain.com',
      password: 'test'
    }).then(function(user) {
      user.username.should.equal('testuser@testdomain.com');
      user.comparePassword('test')
        .then(function(isMatch) {
          isMatch.should.be.true;
        });
      user.comparePassword('notTest')
        .then(function(isMatch) {
          isMatch.should.be.false;
        });
      done();
    });
  });

  it('should create a new user ' + USERS_PATH_BASE + ' POST', function(done) {
    chai.request(server)
      .post(USERS_PATH_BASE)
      .send({'username': 'testuser@testdomain.com', 'password': 'Test'})
      .end(function(err, res) {
        res.should.have.status(201);
        res.should.have.header('location', USERS_PATH_BASE + '/' + res.body.id);
        done();
      });
  });

  it('should login a user ' + USERS_PATH_BASE + '/authenticate POST', function(done) {
    chai.request(server)
    .post(USERS_PATH_BASE)
    .send({'username': 'testuser@testdomain.com', 'password': 'Test'})
    .end(function(err, res) {
      res.should.have.status(201);
      res.should.have.header('location', USERS_PATH_BASE + '/' + res.body.id);
      chai.request(server)
        .post(USERS_PATH_BASE + '/authenticate')
        .send({'username': 'testuser@testdomain.com', 'password': 'Test'})
        .end(function(err, res) {
          res.should.have.status(200);
          token = res.body.token.split('.');
          token.length.should.equal(3);
          done();
        });
    });
  });

  it('should not login a user with incorrect password ' + USERS_PATH_BASE + '/authenticate POST', function(done) {
    chai.request(server)
      .post(USERS_PATH_BASE)
      .send({'username': 'testuser@testdomain.com', 'password': 'Test'})
      .end(function(err, res) {
        res.should.have.status(201);
        res.should.have.header('location', USERS_PATH_BASE + '/' + res.body.id);
        chai.request(server)
          .post(USERS_PATH_BASE + '/authenticate')
          .send({'username': 'testuser@testdomain.com', 'password': 'NotTest'})
          .end(function(err, res) {
            res.should.have.status(401);
            done();
          });
      });
  });

  it('should not login a user with nonexistent username ' + USERS_PATH_BASE + '/authenticate POST', function(done) {
    this.timeout(5000);
    chai.request(server)
      .post(USERS_PATH_BASE)
      .send({'username': 'testuser@testdomain.com', 'password': 'Test'})
      .end(function(err, res) {
        res.should.have.status(201);
        res.should.have.header('location', USERS_PATH_BASE + '/' + res.body.id);
        chai.request(server)
          .post(USERS_PATH_BASE + '/authenticate')
          .send({'username': 'nottestuser@testdomain.com', 'password': 'Test'})
          .end(function(err, res) {
            res.should.have.status(401);
            done();
          });
      });
  });
});
