var $ = require('gulp-load-plugins')();
var argv = require('yargs').argv;
var gulp = require('gulp');
var gls = require('gulp-live-server');
var rimraf = require('rimraf');
var sequence = require('run-sequence');
var sourcemaps = require('gulp-sourcemaps');
var mocha = require('gulp-mocha');
var winston = require('winston');
var eslint = require('gulp-eslint');

// Check for --production flag
var isProduction = Boolean(argv.production);

var PATHS = {
  assets: [
    './client/app/**/*.*',
    '!./client/app/**/*.html',
    '!./client/app/scss/*',
    '!./client/app/**/*.js'
  ],
  templates: [
    './client/app/**/*.html'
  ],
  // Sass will check these folders for files when you use @import.
  sass: [],
  // These files include Foundation for Apps and its dependencies
  vendor: [
    'node_modules/jquery/dist/jquery.js',
    'node_modules/angular/angular.js',
    'node_modules/angular-animate/angular-animate.js',
    'node_modules/angular-aria/angular-aria.js',
    'node_modules/angular-material/angular-material.js',
    'node_modules/angular-messages/angular-messages.js',
    'node_modules/angular-mocks/angular-mocks.js',
    'node_modules/angular-resource/angular-resource.js',
    'node_modules/angular-route/angular-route.js',
    'node_modules/angular-sanitize/angular-sanitize.js',
    'node_modules/angular-ui-router/release/angular-ui-router.js',
    'node_modules/fastclick/lib/fastclick.js'
  ],
  // These files are for your app's JavaScript
  appJS: [
    'client/app/**/*.module.js',
    'client/app/**/*.js'
  ],
  buildDir: './build',
  tests: [
    'test/server/*.js'
  ]
};

gulp.task('test', ['lint'], function() {
  return gulp.src(PATHS.tests)
    .pipe(mocha())
    .once('error', function (err) {
      winston.error(err);
      process.exit(1);
    })
    .once('end', function () {
      process.exit();
    });
});

gulp.task('lint', function() {
  return gulp.src(['**/*.js','!node_modules/**'])
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(eslint.failAfterError());
});

gulp.task('clean', function(cb) {
  rimraf(PATHS.buildDir, cb);
});

gulp.task('copy', function() {
  return gulp.src(PATHS.assets, {
    base: './client'
  })
    .pipe(gulp.dest('./build/app'))
  ;
});

gulp.task('copy:index', function() {
  return gulp.src('./client/index.html')
    .pipe(gulp.dest('./build'))
  ;
});

gulp.task('copy:templates', function() {
  return gulp.src(PATHS.templates)
    .pipe(gulp.dest('./build/app'))
  ;
});

gulp.task('sass', function () {
  var minifyCss = $.if(isProduction, $.minifyCss());

  return gulp.src(['client/app/scss/app.scss', 'node_modules/angular-material/angular-material.scss'])
    .pipe($.sass({
      includePaths: PATHS.sass,
      outputStyle: (isProduction ? 'compressed' : 'nested'),
      errLogToConsole: true
    }))
    .pipe($.autoprefixer({
      browsers: ['last 2 versions', 'ie 10']
    }))
    .pipe(minifyCss)
    .pipe($.concat('app.css'))
    .pipe(gulp.dest('./build/app/css/'))
  ;
});

gulp.task('uglify', ['uglify:vendor', 'uglify:app']);

gulp.task('uglify:vendor', function() {
  var uglify = $.if(isProduction, $.uglify()
    .on('error', function (e) {
      winston.error(e);
    }));

  return gulp.src(PATHS.vendor)
    .pipe(sourcemaps.init())
    .pipe(uglify)
    .pipe($.concat('vendor.js'))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./build/app/'))
  ;
});

gulp.task('uglify:app', function() {
  var uglify = $.if(isProduction, $.uglify()
    .on('error', function (e) {
      winston.error(e);
    }));

  return gulp.src(PATHS.appJS)
    .pipe(sourcemaps.init())
    .pipe(uglify)
    .pipe($.concat('app.js'))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./build/app/'))
  ;
});

gulp.task('build', function(cb) {
  sequence('clean', ['copy', 'sass', 'lint', 'uglify'], 'copy:index','copy:templates', cb);
});

gulp.task('default', ['server'], function() {
  // Watch Sass
  gulp.watch(['./client/app/scss/**/*'], ['sass']);

  // Watch JavaScript
  gulp.watch(PATHS.appJS, ['uglify:app']);

  // Watch static files
  gulp.watch(PATHS.assets, ['copy']);

  // Watch app templates
  gulp.watch(PATHS.templates, ['copy:templates']);

  gulp.watch('./client/index.html', ['copy:index']);
});

gulp.task('server', ['build'], function () {
  // Start the server at the beginning of the task
  var server = gls.new('server/app.js');

  server.start();

  gulp.watch(
    [
      'server/app.js',
      'server/router/routes/*.js',
      'server/models/*.js',
      'server/passportConfig.js',
      'server/securityConfig.js'
    ], function() {
    server.start.bind(server)();
  });
});
