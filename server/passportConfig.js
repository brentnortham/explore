var JwtStrategy = require('passport-jwt').Strategy;
var ExtractJwt = require('passport-jwt').ExtractJwt;
var fs = require('fs');
var models = require('./models/index');

var JWT_PUBLIC_KEY = fs.readFileSync('server/config/keys/jwtkeypub.pem');
var JWT_ALGORITHMS = ['ES256'];
var JWT_ISSUER = 'brentnortham';

module.exports = function(passport) {
  var opts = {};

  opts.secretOrKey = JWT_PUBLIC_KEY;
  opts.algorithms = JWT_ALGORITHMS;
  opts.issuer = JWT_ISSUER;
  opts.jwtFromRequest = ExtractJwt.fromAuthHeader();
  passport.use(new JwtStrategy(opts, function(jwtPayload, done) {
    models.User.findOne({
      id: jwtPayload.sub
    }).then(function(user) {
      if (user) {
        done(null, user);
      } else {
        done(null, false);
      }
    }, function(err) {
      done(err, false);
    });
  }));
};
