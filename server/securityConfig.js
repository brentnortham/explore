var helmet = require('helmet');

module.exports = function (app) {
  app.use(helmet.contentSecurityPolicy({
    directives: {
      defaultSrc: [
        '\'self\''
      ],
      scriptSrc: [
        '\'self\'',
        '\'unsafe-eval\''
      ],
      styleSrc: [
        '\'self\'',
        '\'unsafe-inline\'',
        'https://fonts.googleapis.com'
      ],
      fontSrc: [
        '\'self\'',
        'https://fonts.gstatic.com'
      ],
      imgSrc: [
        '\'self\'',
        'data:'
      ],
      objectSrc: [],
      reportUri: '/reporter'
    },
    setAllHeaders: false,
    disableAndroid: false,
    browserSniff: true
  }));

  app.use(helmet.xssFilter());
  app.use(helmet.frameguard({
    action: 'deny'
  }));
  app.use(helmet.hsts({
    maxAge: 2629746000,
    includeSubdomains: true
  }));
  app.use(helmet.hidePoweredBy());
  app.use(helmet.ieNoOpen());
  app.use(helmet.noSniff());
};
