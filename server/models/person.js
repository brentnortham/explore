'use strict';
module.exports = function(sequelize, DataTypes) {
  var Person = sequelize.define('Person', {
    firstName: DataTypes.TEXT,
    lastName: DataTypes.TEXT,
    bio: DataTypes.TEXT
  }, {
    paranoid: true,
    freezeTableName: true,
    tableName: 'person'
  });

  return Person;
};
