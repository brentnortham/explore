'use strict';

var bcrypt = require('bcrypt');

module.exports = function(sequelize, DataTypes) {
  var BCRYPT_WORK_FACTOR = 12;

  var User = sequelize.define('User', {
    username: {
      type: DataTypes.TEXT,
      allowNull: false,
      unique: true
    },
    password: {
      type: DataTypes.TEXT,
      allowNull: false
    }
  }, {
    freezeTableName: true,
    tableName: 'user',
    instanceMethods: {
      comparePassword: comparePassword
    },
    hooks: {
      beforeCreate: hashPasswordHook,
      beforeUpdate: hashPasswordHook
    }
  });

  function comparePassword(password) {
    var storedPassword = this.password;

    return new sequelize.Promise(function(resolve, reject) {
      bcrypt.compare(password, storedPassword, function(err, isMatch) {
        if (err) {
          reject(err);
        } else {
          resolve(isMatch);
        }
      });
    });
  }

  function hashPasswordHook(user) {
    return new sequelize.Promise(function(resolve, reject) {
      if (!user.changed('password')) {
        return resolve(user);
      }
      bcrypt.hash(user.get('password'), BCRYPT_WORK_FACTOR, function(err, hash) {
        if (err) {
          return reject(err);
        }
        user.set('password', hash);

        return resolve(user);
      });
    });
  }

  return User;
};
