'use strict';
module.exports = function(sequelize, DataTypes) {
  var Account = sequelize.define('Account', {
    name: DataTypes.TEXT
  }, {
    paranoid: true,
    freezeTableName: true,
    tableName: 'account'
  });

  return Account;
};
