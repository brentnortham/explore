'use strict';

module.exports = {
  up: function (queryInterface) {
    return queryInterface.addIndex(
      'user',
      ['username'],
      {
        indexName: 'username_unique_idx',
        indicesType: 'UNIQUE'
      }
    );
  },

  down: function (queryInterface) {
    return queryInterface.removeIndex('user', 'username_unique_idx');
  }
};
