'use strict';

module.exports = {
  up: function (queryInterface) {
    return queryInterface.removeColumn('user', 'deletedAt');
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.removeColumn('user', 'deletedAt', Sequelize.DATE);
  }
};
