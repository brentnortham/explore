'use strict';

module.exports = {
  up: function (queryInterface) {
    return queryInterface.bulkInsert('person', [
      {
        firstName: 'Misty',
        lastName: 'Northam',
        bio: 'A great person all around.',
        createdAt: '20160412',
        updatedAt: '20160412'
      },
      {
        firstName: 'Brent',
        lastName: 'Northam',
        bio: 'The author of this application.',
        createdAt: '20160412',
        updatedAt: '20160412'
      },
      {
        firstName: 'Addison',
        lastName: 'Northam',
        bio: 'A whiz who dances and sings and loves school.',
        createdAt: '20160412',
        updatedAt: '20160412'
      },
      {
        firstName: 'Brynn',
        lastName: 'Northam',
        bio: 'The uncanny source of happiness for everyone in the same room.',
        createdAt: '20160412',
        updatedAt: '20160412'
      },
      {
        firstName: 'Clover',
        lastName: 'Northam',
        bio: 'The newest member of our family.',
        createdAt: '20160412',
        updatedAt: '20160412'
      }
    ], {});
  },

  down: function (queryInterface) {
    return queryInterface.bulkDelete('person', null, {});
  }
};
