module.exports = function (app) {
  app.use('/api/v1/people', require('./routes/people'));
  app.use('/api/v1/users', require('./routes/users'));
  app.use('/reporter', require('./routes/reporter'));
};
