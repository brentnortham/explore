var express = require('express');
var router = express.Router();
var models = require('../../models/index');
var Sequelize = require('sequelize');
var passport = require('passport');

require('../../passportConfig')(passport);

router.post('/', passport.authenticate('jwt', {session: false}), function(req, res) {
  if (!req.body.firstName || !req.body.lastName) {
    res.sendStatus(400);
  } else {
    models.Person.create(req.body).then(function(person) {
      res.status(201);
      res.location('/api/v1/people/' + person.id);
      res.json(person);
    })
    .catch(Sequelize.UniqueConstraintError, function(error) {
      res.status(409).send(error);
    })
    .catch(function(error) {
      res.status(500).send(error);
    });
  }
});

router.get('/', passport.authenticate('jwt', {session: false}), function(req, res) {
  models.Person.findAll({}).then(function(people) {
    if (people) {
      res.json(people);
    } else {
      res.sendStatus(404);
    }
  })
  .catch(function(error) {
    res.status(500).send(error);
  });
});

router.get('/:id', passport.authenticate('jwt', {session: false}), function(req, res) {
  models.Person.find({
    where: {
      id: req.params.id
    }
  })
  .then(function(person) {
    if (person) {
      res.json(person);
    } else {
      res.sendStatus(404);
    }
  })
  .catch(function(error) {
    res.status(500).send(error);
  });
});

router.put('/:id', passport.authenticate('jwt', {session: false}), function(req, res) {
  models.Person.find({
    where: {
      id: req.params.id
    }
  })
  .then(function(person) {
    if (person) {
      person.update(req.body).then(function(person) {
        res.send(person);
      });
    } else {
      res.sendStatus(404);
    }
  })
  .catch(function(error) {
    res.status(500).send(error);
  });
});

router.patch('/:id', passport.authenticate('jwt', {session: false}), function(req, res) {
  models.Person.find({
    where: {
      id: req.params.id
    }
  })
  .then(function(person) {
    if (person) {
      person.update(req.body).then(function(person) {
        res.send(person);
      });
    } else {
      res.sendStatus(404);
    }
  })
  .catch(function(error) {
    res.status(500).send(error);
  });
});

router.delete('/:id', passport.authenticate('jwt', {session: false}), function(req, res) {
  models.Person.destroy({
    where: {
      id: req.params.id
    }
  })
  .then(function(rows) {
    if (rows) {
      res.json(rows);
    } else {
      res.sendStatus(404);
    }
  })
  .catch(function(error) {
    res.status(500).send(error);
  });
});

router.all('/', function(req, res) {
  res.set('Allow', 'GET, POST, PATCH, DELETE');
  res.sendStatus(405);
});

module.exports = router;
