var express = require('express');
var router = express.Router();
var fs = require('fs');
var jwt = require('jsonwebtoken');
var models = require('../../models/index');
var Sequelize = require('sequelize');
var winston = require('winston');

var JWT_PRIVATE_KEY = fs.readFileSync('server/config/keys/jwtkey.pem');
var JWT_OPTIONS = {
  algorithm: 'ES256',
  expiresIn: '30 days',
  issuer: 'brentnortham'
};

var jwtOptions;
var token;

router.post('/', function(req, res) {
  if (!req.body.username || !req.body.password) {
    res.sendStatus(400);
  } else {
    models.User.create(req.body).then(function(user) {
      res.status(201);
      res.location('/api/v1/users/' + user.id);
      user.set('password', null);
      res.send(user);
    })
    .catch(Sequelize.UniqueConstraintError, function(error) {
      res.status(409).send(error);
    })
    .catch(function(error) {
      res.status(500).send(error);
    });
  }
});

router.post('/authenticate', function(req, res) {
  models.User.findOne({
    where: {
      username: req.body.username
    }
  }).then(function(user) {
    if (!user) {
      res.sendStatus(401);
    } else {
      user.comparePassword(req.body.password)
        .then(function (isMatch) {
          if (isMatch) {
            jwtOptions = {
              algorithm: JWT_OPTIONS.algorithm,
              expiresIn: JWT_OPTIONS.expiresIn,
              issuer: JWT_OPTIONS.issuer,
              subject: user.id.toString()
            };

            token = jwt.sign({
              username: user.username
            }, JWT_PRIVATE_KEY, jwtOptions);
            res.status(200);
            res.json({token: 'JWT ' + token});
          } else {
            res.sendStatus(401);
          }
        }, function(err) {
          winston.error(err);
          res.sendStatus(500);
        });
    }
  }, function(err) {
    winston.error(err);
    res.sendStatus(500);
  }
  );
});

router.all('/', function(req, res) {
  res.set('Allow', 'POST');
  res.sendStatus(405);
});

module.exports = router;
