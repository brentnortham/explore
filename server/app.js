var express = require('express');
var bodyParser = require('body-parser');
var morgan = require('morgan');
var passport = require('passport');
var path = require('path');
var winston = require('winston');
var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

// Log to console
app.use(morgan('dev'));

app.use(passport.initialize());

// Configure security settings
require('./securityConfig')(app);

// API Routes
require('./router')(app);

// Client-side
app.use('/app', express.static(path.join(__dirname, '../build/app')));
app.use('/app', function(req, res, next) {
  res.sendStatus(404);
});

app.all('/*', function(req, res, next) {
  res.sendFile(path.join(__dirname, '../build/index.html'));
});

// Error Handling
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
});

app.listen(3000, function () {
  winston.info('Explore app listening on port 3000!');
});

module.exports = app;
