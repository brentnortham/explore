(function() {
  'use strict';

  angular.module('explore.app')
    .run(run);

  run.$inject = ['$rootScope', '$state', 'authService'];

  function run($rootScope, $state, authService) {
    FastClick.attach(document.body);

    $rootScope.$on('$stateChangeStart', function (event, next) {
      if (!authService.isAuthenticated()) {
        if (next.name !== 'login' && next.name !== 'register') {
          event.preventDefault();
          $state.go('login');
        }
      }
    });
  }
})();
