(function() {
  'use strict';

  angular.module('explore.app', [
    'bn.shared',
    'explore.auth',
    'explore.people',
    'explore.user',
    'ngMaterial'
  ]);
})();
