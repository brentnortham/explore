(function() {
  'use strict';

  angular.module('bn.shared')
    .factory('authInterceptor', authInterceptor);

  authInterceptor.$inject = ['$rootScope', '$q', 'AUTH_EVENTS'];

  function authInterceptor($rootScope, $q, AUTH_EVENTS) {
    return {
      responseError: function (response) {
        $rootScope.$broadcast({
          401: AUTH_EVENTS.notAuthenticated
        }[response.status], response);

        return $q.reject(response);
      }
    };
  }
})();
