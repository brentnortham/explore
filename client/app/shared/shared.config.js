(function() {
  'use strict';

  angular.module('bn.shared')
    .config(function($httpProvider) {
      $httpProvider.interceptors.push('authInterceptor');
    });
});
