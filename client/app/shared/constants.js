(function() {
  'use strict';

  angular.module('bn.shared')
    .constant('AUTH_EVENTS', {
      notAuthenticated: 'auth-not-authenticated',
      loggedIn: 'logged-in',
      loggedOut: 'logged-out'
    })
    .constant('API_ENDPOINT', {
      url: 'http://localhost:3000/api/v1'
    });
})();
