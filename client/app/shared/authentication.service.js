(function() {
  'use strict';

  angular.module('bn.shared')
    .factory('authService', authService);

  authService.$inject = ['$q', '$http', '$window', 'API_ENDPOINT', '$rootScope', 'AUTH_EVENTS'];

  function authService($q, $http, $window, API_ENDPOINT, $rootScope, AUTH_EVENTS) {
    var LOCAL_TOKEN_KEY = 'authToken';
    var isAuthenticated = false;
    var authToken;

    var service = {
      register: register,
      login: login,
      logout: logout,
      isAuthenticated: function() {
        return isAuthenticated;
      }
    };

    loadUserCredentials();

    return service;

    function loadUserCredentials() {
      var token = $window.localStorage.getItem(LOCAL_TOKEN_KEY);

      if (token) {
        useCredentials(token);
      }
    }

    function storeUserCredentials(token) {
      $window.localStorage.setItem(LOCAL_TOKEN_KEY, token);
      useCredentials(token);
    }

    function useCredentials(token) {
      $rootScope.$broadcast(AUTH_EVENTS.loggedIn);
      isAuthenticated = true;
      authToken = token;

      $http.defaults.headers.common.Authorization = authToken;
    }

    function destroyUserCredentials() {
      $rootScope.$broadcast(AUTH_EVENTS.loggedOut);
      authToken = undefined;
      isAuthenticated = false;
      $http.defaults.headers.common.Authorization = undefined;
      $window.localStorage.removeItem(LOCAL_TOKEN_KEY);
    }

    function register(user) {
      return $q(function(resolve, reject) {
        $http.post(API_ENDPOINT.url + '/users', user)
          .then(function(response) {
            if (response.status === 201) {
              resolve();
            } else {
              reject();
            }
          });
      });
    }

    function login(user) {
      return $q(function(resolve, reject) {
        $http.post(API_ENDPOINT.url + '/users/authenticate', user).then(function(response) {
          if (response.status === 200) {
            storeUserCredentials(response.data.token);
            resolve();
          } else {
            reject(response.data);
          }
        }, function(response) {
          reject(response.status);
        });
      });
    }

    function logout() {
      destroyUserCredentials();
    }
  }
})();
