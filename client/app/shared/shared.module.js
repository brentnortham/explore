(function() {
  'use strict';

  angular
    .module('bn.shared', [
      'ui.router'
    ]);
})();
