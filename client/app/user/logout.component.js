(function() {
  'use strict';

  angular
    .module('explore.user')
    .component('logout', {
      templateUrl: 'app/user/logout.html',
      controller: 'LogoutController'
    });
})();
