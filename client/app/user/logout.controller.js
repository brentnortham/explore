(function() {
  'use strict';

  angular
    .module('explore.user')
    .controller('LogoutController', LogoutController);

  LogoutController.$inject = ['authService', '$state', '$scope', 'AUTH_EVENTS'];

  function LogoutController(authService, $state, $scope, AUTH_EVENTS) {
    var ctrl = this;

    ctrl.loggedIn = authService.isAuthenticated();
    ctrl.logout = function() {
      authService.logout();
      $state.go('login');
    };

    $scope.$on(AUTH_EVENTS.loggedIn, function () {
      ctrl.loggedIn = true;
    });

    $scope.$on(AUTH_EVENTS.loggedOut, function() {
      ctrl.loggedIn = false;
    });
  }
})();
