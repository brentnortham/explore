(function() {
  'use strict';

  angular
    .module('explore.auth')
    .run(appRun);

  appRun.$inject = ['routerHelper'];

  function appRun(routerHelper) {
    routerHelper.configureStates(getStates(), '/');
  }

  function getStates() {
    return [
      {
        state: 'login',
        config: {
          url: '/login',
          template: '<login class="column"></login>'
        }
      }, {
        state: 'register',
        config: {
          url: '/register',
          template: '<register class="column"></register>'
        }
      }
    ];
  }
})();
