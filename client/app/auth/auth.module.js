(function() {
  'use strict';

  angular
    .module('explore.auth', [
      'bn.shared'
    ]);
})();
