(function() {
  'use strict';

  angular
    .module('explore.auth')
    .component('register', {
      templateUrl: 'app/auth/register.html',
      controller: 'RegisterController'
    });
})();
