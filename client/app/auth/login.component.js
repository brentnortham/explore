(function() {
  'use strict';

  angular
    .module('explore.auth')
    .component('login', {
      templateUrl: 'app/auth/login.html',
      controller: 'LoginController'
    });
})();
