(function() {
  'use strict';

  angular
    .module('explore.auth')
    .controller('LoginController', LoginController);

  LoginController.$inject = ['authService', '$state'];

  function LoginController(authService, $state) {

    var ctrl = this;

    ctrl.user = {
      username: '',
      password: ''
    };

    this.login = function() {
      authService.login(ctrl.user).then(function() {
        $state.go('personList');
      }, function(err) {
        ctrl.error = 'Invalid login, please try again.';
      });
    };
  }
})();
