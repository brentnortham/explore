(function() {
  'use strict';

  angular
    .module('explore.auth')
    .controller('RegisterController', RegisterController);

  RegisterController.$inject = ['authService', '$state'];

  function RegisterController(authService, $state) {

    var ctrl = this;

    ctrl.user = {
      username: '',
      password: ''
    };

    this.register = function() {
      authService.register(ctrl.user).then(function() {
        $state.go('login');
      }, function(err) {
        ctrl.error = err;
      });
    };
  }
})();
