(function() {
  'use strict';

  angular
    .module('explore.people')
    .component('personList', {
      templateUrl: 'app/people/personList.html',
      controller: 'PersonListController'
    });
})();
