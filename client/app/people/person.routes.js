(function() {
  'use strict';

  angular
    .module('explore.people')
    .run(appRun);

  appRun.$inject = ['routerHelper'];

  function appRun(routerHelper) {
    routerHelper.configureStates(getStates(), '/');
  }

  function getStates() {
    return [
      {
        state: 'personList',
        config: {
          url: '/',
          template: '<person-list></person-list>'
        }
      }, {
        state: 'person',
        config: {
          url: '/person/:personId',
          template: '<person-detail></person-detail>'
        }
      }
    ];
  }
})();
