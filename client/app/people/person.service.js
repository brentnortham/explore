(function() {
  'use strict';

  angular
    .module('explore.people')
    .factory('personService', personService);

  personService.$inject = ['$resource', 'API_ENDPOINT'];

  function personService($resource, API_ENDPOINT) {
    return $resource(API_ENDPOINT.url + '/people/:personId');
  }
})();
