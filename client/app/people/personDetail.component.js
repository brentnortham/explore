(function() {
  'use strict';

  angular
    .module('explore.people')
    .component('personDetail', {
      templateUrl: 'app/people/personDetail.html',
      controller: 'PersonDetailController'
    });
})();
