(function() {
  'use strict';

  angular
    .module('explore.people')
    .controller('PersonListController', PersonListController);

  PersonListController.$inject = ['personService'];

  function PersonListController(personService) {
    this.hello = 'Hello World!';
    this.people = personService.query();
  }
})();
