(function() {
  'use strict';

  angular
    .module('explore.people', [
      'ngResource',
      'bn.shared',
      'ngMaterial'
    ]);
})();
