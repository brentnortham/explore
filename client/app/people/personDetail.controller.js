(function() {
  'use strict';

  angular
    .module('explore.people')
    .controller('PersonDetailController', PersonDetailController);

  PersonDetailController.$inject = ['$stateParams', 'personService'];

  function PersonDetailController($stateParams, personService) {
    this.person = personService.get({personId: $stateParams.personId});
  }
})();
